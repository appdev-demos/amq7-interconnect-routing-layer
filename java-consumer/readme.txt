How to
------

To compile the code run:

   mvn clean package dependency:copy-dependencies -DincludeScope=runtime


To run the consumer:

   java -cp "target/classes/:target/dependency/*" org.apache.activemq.artemis.jms.example.AMQPQueueConsumer


The consumer will attempt to connect to the router and wait for messages to be available until interrupted.
