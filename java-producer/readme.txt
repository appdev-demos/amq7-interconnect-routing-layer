How to
------

To compile the code run:

   mvn clean package dependency:copy-dependencies -DincludeScope=runtime


To run the producer:

   java -cp "target/classes/:target/dependency/*" org.apache.activemq.artemis.jms.example.AMQPQueueProducer


It will be default send 1 message.
You can pass a numeric argument to specify the number of messages, for example 10:

   java -cp "target/classes/:target/dependency/*" org.apache.activemq.artemis.jms.example.AMQPQueueProducer 10
