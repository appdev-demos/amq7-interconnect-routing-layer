Camel QPID AMQP JMS consumer
============================

Tested with:

	> mvn -version
	
	Apache Maven 3.3.9 (bb52d8502b132ec0a5a3f4c09453c07478323dc5; 2015-11-10T16:41:47+00:00)
	Maven home: /home/developer/apps/maven/apache-maven-3.3.9
	Java version: 1.8.0_73, vendor: Oracle Corporation
	Java home: /home/developer/apps/java/jdk1.8.0_73/jre
	Default locale: en_GB, platform encoding: UTF-8
	OS name: "linux", version: "3.10.0-327.10.1.el7.x86_64", arch: "amd64", family: "unix"


1) build and install the project with Maven:

   > mvn clean install

2) run Fuse's processes standalone:

   > mvn camel:run



