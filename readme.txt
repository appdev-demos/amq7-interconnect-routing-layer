AMQ-7 Interconnect Demo
=======================

This collection of code and resources aim to demonstrate how to combine Interconnect with Artemis (both AMQ-7).

The different blocks should be combined in the following shape:

	Clients (producers/consumers) --> Routing layer (Interconnect) --> Brokers (Artemis)


The project contains:

 - A Java Client producer
 - A Java Client consumer
 - A Camel Client consumer
 - Interconnect configuration files
 - Slides describing the scenarios presented (PowerPoint)


Pre-Requirements:

 > To have 2 instances of Artemis installed and ready.
 > To have Interconnect installed
 > The usual stuff (Java, Maven, ...)


AMQ-7 Red Hat Documentation:

	https://access.redhat.com/documentation/en/red-hat-jboss-amq/?version=7.0-beta




Handy Interconnect Management commands:
---------------------------------------

	This sample command shows all the nodes in the routing layer:

		> qdstat -b 0.0.0.0:5602 -n
			Routers in the Network
			  router-id  next-hop  link
			  ===========================
			  Clients    (self)    -
			  R1         -         0
			  R2         -         1


	This sample command shows 2 brokers (route-container) connected to the router:

		> qdstat -b 0.0.0.0:5692 -c | grep route-container
		  1   localhost:5662                                         route-container  out  no-security  anonymous-user
		  3   localhost:5672                                         route-container  out  no-security  anonymous-user


	This sample command shows how 10 messages were balanced (5/5) between the available broker:

		> qdstat -b 0.0.0.0:5692 -l
		Router Links
		  type      dir  conn id  id  peer  class   addr                      phs  cap  undel  unsett  del  presett  acc  rej  rel  mod  admin    oper
		  ==============================================================================================================================================
		  endpoint  in   3        6         mobile  jms.queue.myorder.token7  1    250  0      0       0    0        0    0    0    0    enabled  up
		  endpoint  out  3        7         mobile  jms.queue.myorder.token7  0    250  0      0       5    0        5    0    0    0    enabled  up
		  endpoint  in   2        8         mobile  jms.queue.myorder.token7  1    250  0      0       0    0        0    0    0    0    enabled  up
		  endpoint  out  2        9         mobile  jms.queue.myorder.token7  0    250  0      0       5    0        5    0    0    0    enabled  up
		  endpoint  in   22       15        mobile  $management               0    250  0      0       1    0        1    0    0    0    enabled  up
		  endpoint  out  22       16        local   temp.5Q1_aWkIRIMQ5pP           250  0      0       0    0        0    0    0    0    enabled  up
	
